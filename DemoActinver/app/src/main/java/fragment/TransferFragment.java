package fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nubaj.demoactinver.ConfirmActivity;
import com.nubaj.demoactinver.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adapter.SpinAdapterDestinationContract;
import adapter.SpinAdapterOriginContract;
import implementation.ContractNothingSelectedSpinnerAdapter;
import implementation.FunctionJson;
import model.ChallengeResponse;
import model.Datum;
import model.Datum2;
import model.DestinationAccountResponse;
import model.TransferRequest;
import utils.GlobalVariables;
import utils.InternetConnection;

public class TransferFragment extends Fragment
{
    View rootView;
    Spinner originContractSpinner, destinationContractSpinner;
    TextInputLayout textInputLayoutAmount, textInputLayoutDescriptionOperation;
    TextView textViewTitleDestination, textViewTitleEmail, textViewEmail;
    EditText editTextAmount, editTextDescriptionOperation;
    Button buttonContinue;
    List<Datum> datumList;
    SpinAdapterOriginContract spinAdapterOriginContract;
    Datum datum;
    Datum2 datum2;
    String contract, response, id;
    boolean step2 = false, step3=false;
    FunctionJson functionJson;
    HashMap<String, String> hashMapDestinationContract, hashMapTransfer, hashMapChallenge;
    Gson gson;
    static int position1 =0, REQUEST_CODE=1;
    public TransferFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_transfer,container,false);
        originContractSpinner = (Spinner) rootView.findViewById(R.id.spinnerOriginContract);
        textViewTitleDestination = (TextView) rootView.findViewById(R.id.textViewTitleStep2);
        destinationContractSpinner = (Spinner) rootView.findViewById(R.id.spinnerDestinationContract);
        editTextAmount = (EditText) rootView.findViewById(R.id.editTextAmount);
        textViewTitleEmail = (TextView) rootView.findViewById(R.id.textViewTitleEmail);
        textViewEmail = (TextView) rootView.findViewById(R.id.textViewEmail);
        editTextDescriptionOperation = (EditText) rootView.findViewById(R.id.editTextDescriptionOperation);
        textInputLayoutAmount = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutAmount);
        textInputLayoutDescriptionOperation = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutDescriptionOperation);
        buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                View focusView = null;
                focusView = getActivity().getCurrentFocus();
                // Reset errors.
                editTextAmount.setError(null);
                editTextDescriptionOperation.setError(null);
                if (focusView != null)
                {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                }
                if(!TextUtils.isEmpty(editTextAmount.getText().toString()) && !TextUtils.isEmpty(editTextDescriptionOperation.getText().toString()))
                {
                    if(datum2.getAccountType().equals("CLABE"))
                    {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Confirmación Transferencia")
                                .setMessage("¿Está seguro de realizar la transferencia?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        if(InternetConnection.checkConnection(getActivity()))
                                        {
                                            new TransferFragment.enviar_informacion2().execute();
                                        }
                                        else
                                        {
                                            Toast.makeText(getActivity(),getString(R.string.error_internet),Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(),getActivity().getString(R.string.error_transfer),Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {

                    if(TextUtils.isEmpty(editTextAmount.getText().toString()))
                    {
                        editTextAmount.setError(getString(R.string.error_amount));
                        focusView = editTextAmount;
                    }
                    else
                    {
                        editTextDescriptionOperation.setError(getString(R.string.error_description_operation));
                        focusView = editTextDescriptionOperation;
                    }
                }
            }
        });
        SpinAdapterOriginContract customAdapter = new SpinAdapterOriginContract(getActivity(),GlobalVariables.asesoradosResponse.getData());
        originContractSpinner.setAdapter(customAdapter);
        originContractSpinner.setAdapter(
                new ContractNothingSelectedSpinnerAdapter(
                        customAdapter,
                        R.layout.contract_spinner_row_nothing,
                        getActivity()));
        originContractSpinner.setOnItemSelectedListener(mOriginContractSelectedListener);
        step2 = false;
        step3=false;
        return rootView;
    }

    private AdapterView.OnItemSelectedListener mOriginContractSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
        {
            if(step2==true)
            {
                position1=position;
                new TransferFragment.enviar_informacion().execute();
                String x = "";
            }
            else
            {
                step2 = true;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener mDestinationContractSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
        {
            if(step3==true)
            {
                datum2 = new Datum2();
                try
                {
                    if(GlobalVariables.destinationAccountResponse.getData().get(position-1)!=null)
                    {
                        datum2 = GlobalVariables.destinationAccountResponse.getData().get(position - 1);
                    }
                    else
                    {
                        datum2 = GlobalVariables.destinationAccountResponse.getData().get(position);
                    }
                    visibilityStep3(datum2);
                }
                catch (Exception e)
                {
                    e.getStackTrace();
                }
            }
            else
            {
                step3 = true;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    void visibilityStep3(Datum2 datum2)
    {
        textInputLayoutAmount.setVisibility(View.VISIBLE);
        textViewTitleEmail.setVisibility(View.VISIBLE);
        textViewEmail.setVisibility(View.VISIBLE);
        textInputLayoutDescriptionOperation.setVisibility(View.VISIBLE);
        buttonContinue.setVisibility(View.VISIBLE);
        try
        {
            textViewEmail.setText(datum2.getBeneficiaryMail());
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    public class enviar_informacion extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Obteniendo contratos de destino");
            pDialog.setMessage("Espere un momento mientras se obtienen los contratos de destino...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onDestination();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            if(GlobalVariables.destinationAccountResponse!=null)
            {
                if (GlobalVariables.destinationAccountResponse.isSuccess() == true)
                {
                    fillDestinationContract();
                }
            }
            pDialog.dismiss();

        }

        void onDestination()
        {
            datum = new Datum();
            datum = GlobalVariables.asesoradosResponse.getData().get(position1-1);
            response = "";
            functionJson = new FunctionJson();
            hashMapDestinationContract = new HashMap<String, String>();
            response = functionJson.RequestHttp(getString(R.string.url_destination_contract) + String.valueOf(datum.getContract()) + "&access_token=" + GlobalVariables.loginResponse.getAccess_token().toString() + "&accountStatus=" + "1", FunctionJson.GET, hashMapDestinationContract);
            gson = new GsonBuilder().create();
            try
            {
                GlobalVariables.destinationAccountResponse = new DestinationAccountResponse();
                GlobalVariables.destinationAccountResponse = gson.fromJson(response, DestinationAccountResponse.class);
                if(GlobalVariables.destinationAccountResponse.isSuccess()==true)
                {

                }
                else
                {
                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(getActivity(),GlobalVariables.destinationAccountResponse.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
        }

        void fillDestinationContract()
        {
            try
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        textViewTitleDestination.setVisibility(View.VISIBLE);
                        destinationContractSpinner.setVisibility(View.VISIBLE);
                        List<Datum2> datum2List = new ArrayList<Datum2>();
                        for(Datum2 datum2: GlobalVariables.destinationAccountResponse.getData())
                        {
                            if(datum2.getAccountType().equals("CLABE"))
                            {
                                datum2List.add(datum2);
                            }
                        }
                        final SpinAdapterDestinationContract destinationContract = new SpinAdapterDestinationContract(getActivity(), datum2List);

                        destinationContractSpinner.setAdapter(destinationContract);
                        destinationContractSpinner.setAdapter(
                                new ContractNothingSelectedSpinnerAdapter(
                                        destinationContract,
                                        R.layout.destination_spinner_row_nothing,
                                        getActivity()));
                    }
                });

                destinationContractSpinner.setOnItemSelectedListener(mDestinationContractSelectedListener);
            }
            catch (Exception e)
            {
                e.getStackTrace();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }

    public class enviar_informacion2 extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Obteniendo Challenge");
            pDialog.setMessage("Espere un momento mientras se obtiene el challenge...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            if(GlobalVariables.tokenResponse.getData().equals("SOFT"))
            {
                /*getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        GlobalVariables.transferRequest = new TransferRequest();
                        GlobalVariables.transferRequest.setAccountType(Integer.valueOf(datum.getType()));
                        GlobalVariables.transferRequest.setAmount(Integer.valueOf(editTextAmount.getText().toString()));
                        GlobalVariables.transferRequest.setComments(editTextDescriptionOperation.getText().toString());
                        GlobalVariables.transferRequest.setCurrency("MXN");
                        GlobalVariables.transferRequest.setDestinationAccount(datum2.getAccountNumber().toString());
                        GlobalVariables.transferRequest.setTransferDetails(editTextDescriptionOperation.getText().toString());
                        GlobalVariables.transferRequest.setContract(String.valueOf(datum.getContract()));
                        GlobalVariables.transferRequest.setBank(datum2.getBankID().toString());
                        GlobalVariables.transferRequest.setAlias(datum.getName().toString());
                        GlobalVariables.transferRequest.setBank_name(datum2.getBankName().toString());
                        GlobalVariables.transferRequest.setDestination_name(datum2.getBeneficiaryName().toString());
                        Intent intent = new Intent(getActivity(), ConfirmActivity.class);
                        startActivity(intent);
                    }
                });*/
                onChallenge();
            }
            else
            {
                onChallenge();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();
        }

        void onChallenge()
        {
            functionJson = new FunctionJson();
            hashMapChallenge = new HashMap<String, String>();
            response = functionJson.RequestHttp(getString(R.string.url_challenge) + GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapChallenge);
            gson = new GsonBuilder().create();
            try
            {
                GlobalVariables.challengeResponse = new ChallengeResponse();
                GlobalVariables.challengeResponse = gson.fromJson(response,ChallengeResponse.class);
                if(GlobalVariables.challengeResponse.isSuccess()==true)
                {
                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            GlobalVariables.transferRequest = new TransferRequest();
                            GlobalVariables.transferRequest.setAccountType(datum2.getAccountType());
                            GlobalVariables.transferRequest.setAmount(Integer.valueOf(editTextAmount.getText().toString()));
                            GlobalVariables.transferRequest.setComments(editTextDescriptionOperation.getText().toString());
                            GlobalVariables.transferRequest.setCurrency("MXN");
                            GlobalVariables.transferRequest.setDestinationAccount(datum2.getAccountNumber().toString());
                            GlobalVariables.transferRequest.setTransferDetails(editTextDescriptionOperation.getText().toString());
                            GlobalVariables.transferRequest.setContract(String.valueOf(datum.getContract()));
                            GlobalVariables.transferRequest.setBank(datum2.getBankID().toString());
                            GlobalVariables.transferRequest.setAlias(datum.getName().toString());
                            GlobalVariables.transferRequest.setBank_name(datum2.getBankName().toString());
                            GlobalVariables.transferRequest.setDestination_name(datum2.getBeneficiaryName().toString());
                            Intent intent = new Intent(getActivity(), ConfirmActivity.class);
                            startActivity(intent);
                        }
                    });
                }
                else
                {
                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(getActivity(),GlobalVariables.challengeResponse.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
        }


    }


}
