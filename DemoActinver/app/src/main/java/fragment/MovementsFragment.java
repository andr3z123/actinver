package fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nubaj.demoactinver.R;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import adapter.CustomArrayAdapter;
import implementation.ContractNothingSelectedSpinnerAdapter;
import implementation.FunctionJson;
import model.AsesoradosResponse;
import model.CashAccount;
import model.Datum;
import model.Investment;
import model.Movements;
import model.MovementsResponse;
import utils.GlobalVariables;
import utils.InternetConnection;

public class MovementsFragment extends Fragment
{
    private String response="", asesorado="";
    private FunctionJson functionJson;
    private Spinner spinnerAsesorados;
    private TextView textViewDateStart, textViewDateEnd;
    private DatePickerDialog fromDatePickerDialog, toDatePickerDialog;
    private Button buttonSearch;
    private int msYear, msMonth, msDay, msHour, msMinute, msYearEnd, msMonthEnd, msDayEnd;
    private SimpleDateFormat dateFormatter;
    private HashMap<String, String> hashMapAsesorados;
    private Gson gson;
    private List<CashAccount> cashAccoutList;
    private ListView listView;
    private Movements movementsObject;
    private List<Movements> movementsList;

    public MovementsFragment()
    {
        // Required empty public constructor
    }

    private RecyclerView mRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.fragment_movements, null, false);
        spinnerAsesorados = (Spinner) root.findViewById(R.id.spinnerAsesorado);
        listView = (ListView) root.findViewById(R.id.listView1);

        textViewDateStart = (TextView) root.findViewById(R.id.textViewDateStart);
        textViewDateEnd = (TextView) root.findViewById(R.id.textViewDateEnd);
        hashMapAsesorados = new HashMap<String, String>();
        functionJson = new FunctionJson();

        if(!TextUtils.isEmpty(GlobalVariables.loginResponse.getAccess_token()))
        {
            new MovementsFragment.enviar_informacion().execute();
        }
        else
        {
            Toast.makeText(getActivity(),getString(R.string.error_contract),Toast.LENGTH_LONG).show();
        }

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        textViewDateStart.setText(dateFormatter.format(new Date()));
        textViewDateEnd.setText(dateFormatter.format(new Date()));
        textViewDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateStart();
            }
        });

        textViewDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateEnd();
            }
        });
        buttonSearch = (Button) root.findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!TextUtils.isEmpty(asesorado))
                {

                    if(InternetConnection.checkConnection(getActivity()))
                    {
                        new MovementsFragment.enviar_informacion2().execute();
                    }
                    else
                    {
                        Toast.makeText(getActivity(),getString(R.string.error_internet),Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(),getString(R.string.error_contract_select),Toast.LENGTH_LONG).show();
                }
            }
        });

        return root;
    }

    public class enviar_informacion extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Obteniendo contratos");
            pDialog.setMessage("Espere un momento mientras se obtienen los contratos...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onContract();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        void onContract()
        {
            response = functionJson.RequestHttp(getString(R.string.url_asesorados) + GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapAsesorados);
            gson = new GsonBuilder().create();
            GlobalVariables.asesoradosResponse = new AsesoradosResponse();
            GlobalVariables.asesoradosResponse = gson.fromJson(response, AsesoradosResponse.class);
            List<String> asesorados_list = new ArrayList<String>();
            if (GlobalVariables.asesoradosResponse.getData() != null)
            {
                for (Datum datum : GlobalVariables.asesoradosResponse.getData())
                {
                    String cont = String.valueOf(datum.getContract());
                    asesorados_list.add(cont);
                    cont = "";
                }


                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asesorados_list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        spinnerAsesorados.setAdapter(adapter);
                        spinnerAsesorados.setAdapter(
                                new ContractNothingSelectedSpinnerAdapter(
                                        adapter,
                                        R.layout.asesorado_spinner_row_nothing_selected,
                                        getActivity()));
                    }
                });

                spinnerAsesorados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        asesorado = (String) adapterView.getItemAtPosition(i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
            else
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(getActivity(), getString(R.string.error_contract), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    public class enviar_informacion2 extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Consultando movimientos");
            pDialog.setMessage("Espere un momento mientras se obtienen los movimientos...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onMovements();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        void onMovements()
        {
            response = "";
            functionJson = new FunctionJson();
            hashMapAsesorados = new HashMap<String, String>();
            response = functionJson.RequestHttp(getString(R.string.url_movements) + asesorado + "?access_token=" + GlobalVariables.loginResponse.getAccess_token().toString() + "&start=" + textViewDateStart.getText().toString() + "&end=" + textViewDateEnd.getText().toString(), FunctionJson.GET, hashMapAsesorados);
            gson = new GsonBuilder().create();
            GlobalVariables.movementsResponse = new MovementsResponse();
            GlobalVariables.movementsResponse = gson.fromJson(response, MovementsResponse.class);
            fillAdapter();
            final CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(), R.layout.fragment_card_view, movementsList);
            try
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        //Toast.makeText(getActivity().getApplicationContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();

                    }
                });
            }
            catch (Exception e)
            {
                e.getStackTrace();
            }
        }
    }

    void fillAdapter()
    {
        Double finalBalance,withDrawal;
        movementsObject = new Movements();
        movementsList = new ArrayList<Movements>();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("USD"));
        format.setMinimumFractionDigits(2);
        try
        {
            if(GlobalVariables.movementsResponse.getData().getCashAccount().size()>0)
            {
                for (CashAccount cashAccount:GlobalVariables.movementsResponse.getData().getCashAccount())
                {
                    movementsObject.setTransactionDate(cashAccount.getTransactionDate());
                    movementsObject.setTransactionTime(cashAccount.getTransactionTime());
                    movementsObject.setDescription(cashAccount.getDescription());
                    finalBalance = Double.valueOf(cashAccount.getFinalBalance().toString());
                    movementsObject.setFinalBalance(format.format(finalBalance));
                    movementsObject.setDeposit(cashAccount.getDeposit());
                    if(!TextUtils.isEmpty(cashAccount.getWithdrawal().toString()))
                    {
                        withDrawal = Double.valueOf(cashAccount.getWithdrawal().toString());
                    }
                    else
                    {
                        withDrawal = 0.0;
                    }
                    movementsObject.setWithdrawal(format.format(withDrawal));
                    movementsList.add(movementsObject);
                    movementsObject = new Movements();
                }
            }

            if(GlobalVariables.movementsResponse.getData().getInvestments().size()>0)
            {
                for (Investment invesment:GlobalVariables.movementsResponse.getData().getInvestments())
                {
                    movementsObject.setTransactionDate(invesment.getTransactionDate());
                    movementsObject.setTransactionTime(invesment.getTransactionTime());
                    movementsObject.setDescription(invesment.getDescription());
                    finalBalance = Double.valueOf(invesment.getFinalBalance().toString());
                    movementsObject.setFinalBalance(format.format(finalBalance));
                    movementsObject.setDeposit(invesment.getDeposit());
                    if(!TextUtils.isEmpty(invesment.getWithdrawal().toString()))
                    {
                        withDrawal = Double.valueOf(invesment.getWithdrawal().toString());
                    }
                    else
                    {
                        withDrawal = 0.0;
                    }
                    movementsObject.setWithdrawal(format.format(withDrawal));
                    movementsObject.setLiquidationDate(invesment.getLiquidationDate());
                    movementsObject.setOperationReference(invesment.getOperationReference());
                    movementsObject.setIssuerName(invesment.getIssuerName());
                    movementsObject.setTitles(invesment.getTitles());
                    movementsObject.setPrice(invesment.getPrice());
                    movementsObject.setTerm(invesment.getTerm());
                    movementsObject.setInterestRate(invesment.getInterestRate());
                    movementsObject.setCommissionAmount(invesment.getCommissionAmount());
                    movementsObject.setVat(invesment.getVat());
                    movementsObject.setIncomeTax(invesment.getIncomeTax());
                    movementsObject.setConcept(invesment.getConcept());

                    movementsList.add(movementsObject);
                    movementsObject = new Movements();
                }
            }

            if(movementsList.size()==0)
            {
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(getActivity(),getString(R.string.error_movements),Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    private void setDateStart()
    {

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;
                textViewDateStart.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }

    private void setDateEnd()
    {
        Calendar newCalendar = Calendar.getInstance();

        toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                msYearEnd = year;
                msMonthEnd = monthOfYear;
                msDayEnd = dayOfMonth;
                textViewDateEnd.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        toDatePickerDialog.show();
    }
}
