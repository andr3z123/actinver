package model;

/**
 * Created by andresaleman on 3/6/17.
 */

public class OthersAccountsBankThirdpartyResponse
{
    private String code;
    private boolean success;
    private String type;
    private String message;
    private ErrorTransferResponse error;
    private Data3 data;

    public Data3 getData() {
        return data;
    }

    public void setData(Data3 data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ErrorTransferResponse getError() {
        return error;
    }

    public void setError(ErrorTransferResponse error) {
        this.error = error;
    }
}
