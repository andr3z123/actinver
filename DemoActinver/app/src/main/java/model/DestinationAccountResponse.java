package model;

import java.util.List;

/**
 * Created by andresaleman on 2/27/17.
 */

public class DestinationAccountResponse
{
    public DestinationAccountResponse()
    {

    }

    private String code;
    private List<Datum2> data;
    private boolean success;
    private String type;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Datum2> getData() {
        return data;
    }

    public void setData(List<Datum2> data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
