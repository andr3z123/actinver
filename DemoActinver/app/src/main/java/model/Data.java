package model;

import java.util.List;

/**
 * Created by andresaleman on 2/24/17.
 */

public class Data
{
    public Data()
    {

    }

    private List<CashAccount> cashAccount;
    private List<Investment> investments;

    public List<CashAccount> getCashAccount() {
        return cashAccount;
    }

    public void setCashAccount(List<CashAccount> cashAccount) {
        this.cashAccount = cashAccount;
    }

    public List<Investment> getInvestments() {
        return investments;
    }

    public void setInvestments(List<Investment> investments) {
        this.investments = investments;
    }
}
