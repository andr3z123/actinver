package model;

/**
 * Created by andresaleman on 2/24/17.
 */

public class Currency2
{
    private String abbr;
    private String value;

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
