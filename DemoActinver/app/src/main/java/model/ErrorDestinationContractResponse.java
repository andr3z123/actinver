package model;

/**
 * Created by andresaleman on 2/27/17.
 */

public class ErrorDestinationContractResponse
{
    public ErrorDestinationContractResponse()
    {

    }

    private String code;
    private boolean success;
    private String type;
    private String message;
    private Error error;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
