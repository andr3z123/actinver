package model;

/**
 * Created by andresaleman on 2/28/17.
 */

public class Data2
{
    public Data2()
    {

    }

    private String tokenChallenge;

    public String getTokenChallenge()
    {
        return tokenChallenge;
    }
    public void setTokenChallenge(String tokenChallenge)
    {
        this.tokenChallenge = tokenChallenge;
    }
}
