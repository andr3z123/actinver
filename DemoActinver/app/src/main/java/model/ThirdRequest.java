package model;

/**
 * Created by andresaleman on 2/28/17.
 */

public class ThirdRequest 
{
    public ThirdRequest()
    {

    }

    private String destinationAccount;
    private int transferAmount;
    private String transferCurrency;
    private String transferDetails;
    private String comments;
    private String newValue;
    private String bank;
    private String accountType;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCsrfmiddlewaretoken() {
        return csrfmiddlewaretoken;
    }

    public void setCsrfmiddlewaretoken(String csrfmiddlewaretoken) {
        this.csrfmiddlewaretoken = csrfmiddlewaretoken;
    }

    private String csrfmiddlewaretoken;

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public int getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(int transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransferCurrency() {
        return transferCurrency;
    }

    public void setTransferCurrency(String transferCurrency) {
        this.transferCurrency = transferCurrency;
    }

    public String getTransferDetails() {
        return transferDetails;
    }

    public void setTransferDetails(String transferDetails) {
        this.transferDetails = transferDetails;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
}
