package model;

/**
 * Created by andresaleman on 3/6/17.
 */

public class ErrorTransferResponse
{
    public  ErrorTransferResponse()
    {

    }

    private String responseSystemCode;
    private String responseMessage;
    private String responseType;
    private String responseCategory;

    public String getResponseSystemCode() {
        return responseSystemCode;
    }

    public void setResponseSystemCode(String responseSystemCode) {
        this.responseSystemCode = responseSystemCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getResponseCategory() {
        return responseCategory;
    }

    public void setResponseCategory(String responseCategory) {
        this.responseCategory = responseCategory;
    }
}
