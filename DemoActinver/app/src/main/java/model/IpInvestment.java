package model;

import java.util.List;

/**
 * Created by andresaleman on 2/24/17.
 */

public class IpInvestment
{
    public IpInvestment()
    {

    }

    private String name;
    private double total;
    private List<Currency2> currencies;

    public List<Currency2> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency2> currencies) {
        this.currencies = currencies;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
