package model;

/**
 * Created by andresaleman on 2/27/17.
 */

public class Datum2
{
    public Datum2()
    {

    }

    private String accountNumber;
    private String settlementInstructionID;
    private String accountType;
    private String bankName;
    private String destinationAccount;
    private String clientID;
    private String contractNumber;
    private int companyID;
    private String alias;
    private int destinationAccountType;
    private String bankID;
    private Double maxAmount;
    private String beneficiaryMail;
    private String countryCodeNumber;
    private String phoneNumber;
    private String accountStatus;
    private String beneficiaryName;
    private String registerKey;
    private Object availabilityDate;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSettlementInstructionID() {
        return settlementInstructionID;
    }

    public void setSettlementInstructionID(String settlementInstructionID) {
        this.settlementInstructionID = settlementInstructionID;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getDestinationAccountType() {
        return destinationAccountType;
    }

    public void setDestinationAccountType(int destinationAccountType) {
        this.destinationAccountType = destinationAccountType;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public Double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getBeneficiaryMail() {
        return beneficiaryMail;
    }

    public void setBeneficiaryMail(String beneficiaryMail) {
        this.beneficiaryMail = beneficiaryMail;
    }

    public String getCountryCodeNumber() {
        return countryCodeNumber;
    }

    public void setCountryCodeNumber(String countryCodeNumber) {
        this.countryCodeNumber = countryCodeNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getRegisterKey() {
        return registerKey;
    }

    public void setRegisterKey(String registerKey) {
        this.registerKey = registerKey;
    }

    public Object getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(Object availabilityDate) {
        this.availabilityDate = availabilityDate;
    }
}
