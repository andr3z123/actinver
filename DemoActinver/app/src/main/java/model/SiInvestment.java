package model;

import java.util.List;

/**
 * Created by andresaleman on 2/24/17.
 */

public class SiInvestment
{
    public SiInvestment()
    {

    }

    private String name;
    private double total;
    private List<Currency> currencies;

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
