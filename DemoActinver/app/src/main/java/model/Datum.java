package model;

/**
 * Created by andresaleman on 2/24/17.
 */

public class Datum
{
    public Datum()
    {

    }

    private int contract;
    private String name;
    private String type;
    private String surname;
    private String toolTip;
    private int contractType;
    private String cash;
    private String investments;
    private String total;
    private String serviceType;
    private Object settlementCash;
    private Object txCash;
    private SiInvestment siInvestment;
    private Object mcInvestment;
    private Object mdInvestment;
    private IpInvestment ipInvestment;

    public int getContract() {
        return contract;
    }

    public void setContract(int contract) {
        this.contract = contract;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public int getContractType() {
        return contractType;
    }

    public void setContractType(int contractType) {
        this.contractType = contractType;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getInvestments() {
        return investments;
    }

    public void setInvestments(String investments) {
        this.investments = investments;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Object getSettlementCash() {
        return settlementCash;
    }

    public void setSettlementCash(Object settlementCash) {
        this.settlementCash = settlementCash;
    }

    public Object getTxCash() {
        return txCash;
    }

    public void setTxCash(Object txCash) {
        this.txCash = txCash;
    }

    public SiInvestment getSiInvestment() {
        return siInvestment;
    }

    public void setSiInvestment(SiInvestment siInvestment) {
        this.siInvestment = siInvestment;
    }

    public Object getMcInvestment() {
        return mcInvestment;
    }

    public void setMcInvestment(Object mcInvestment) {
        this.mcInvestment = mcInvestment;
    }

    public Object getMdInvestment() {
        return mdInvestment;
    }

    public void setMdInvestment(Object mdInvestment) {
        this.mdInvestment = mdInvestment;
    }

    public IpInvestment getIpInvestment() {
        return ipInvestment;
    }

    public void setIpInvestment(IpInvestment ipInvestment) {
        this.ipInvestment = ipInvestment;
    }
}
