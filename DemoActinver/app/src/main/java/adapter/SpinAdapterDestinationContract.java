package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nubaj.demoactinver.R;

import java.util.List;

import model.Datum2;

/**
 * Created by andresaleman on 2/27/17.
 */

public class SpinAdapterDestinationContract extends BaseAdapter
{
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Datum2> values;
    LayoutInflater inflter;

    public SpinAdapterDestinationContract(Context context, List<Datum2> values)
    {
        this.context = context;
        this.values = values;
        inflter = (LayoutInflater.from(context));
    }

    public int getCount(){
        return values.size();
    }

    public Datum2 getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.spinner_destination_contract, null);
        TextView textName = (TextView) view.findViewById(R.id.textViewName);
        TextView textAccountNumber = (TextView) view.findViewById(R.id.textViewAccountNumber);
        TextView textBank = (TextView) view.findViewById(R.id.textViewBank);

        textName.setText(values.get(i).getBeneficiaryName());
        textAccountNumber.setText(values.get(i).getAccountNumber());
        textBank.setText(values.get(i).getBankName());
        return view;
    }
}
