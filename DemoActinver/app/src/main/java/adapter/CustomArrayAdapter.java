package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nubaj.demoactinver.R;

import java.util.List;

import model.Movements;

/**
 * Created by andresaleman on 2/24/17.
 */

public class CustomArrayAdapter extends ArrayAdapter<Movements>
{
    Context context;
    int layoutResourceId;
    List<Movements> data = null;
    CashAccountArrayHolder holder = null;

    public CustomArrayAdapter(Context context, int layoutResourceId, List<Movements> data)
    {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent)
    {
        View row = convertView;
        final Movements movements = data.get(position);

        if(row==null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CashAccountArrayHolder();
            holder.textViewTitleDate = (TextView) row.findViewById(R.id.textViewTitleDate);
            holder.textViewDate = (TextView) row.findViewById(R.id.textViewTitleDate);
            holder.textViewTitleDescription = (TextView) row.findViewById(R.id.textViewTitleDescription);
            holder.textViewDescription = (TextView) row.findViewById(R.id.textViewDescription);
            holder.textViewTitleAmount = (TextView) row.findViewById(R.id.textViewTitleAmount);
            holder.textViewAmount = (TextView) row.findViewById(R.id.textViewAmount);
            holder.textViewTitleFinalBalance = (TextView) row.findViewById(R.id.textViewAccountNumber);
            holder.textViewFinalBalance = (TextView) row.findViewById(R.id.textViewBalance);
        }
        else
        {
            holder = (CashAccountArrayHolder)row.getTag();
        }


        if(holder!=null)
        {
            holder.textViewDate.setText(data.get(position).getTransactionDate().toString());
            holder.textViewDescription.setText(data.get(position).getDescription().toString());
            holder.textViewAmount.setText(data.get(position).getWithdrawal().toString());
            holder.textViewFinalBalance.setText(data.get(position).getFinalBalance().toString());
        }

        return row;
    }

    static class CashAccountArrayHolder
    {
        TextView textViewTitleDate;
        TextView textViewDate;
        TextView textViewTitleDescription;
        TextView textViewDescription;
        TextView textViewTitleAmount;
        TextView textViewAmount;
        TextView textViewTitleFinalBalance;
        TextView textViewFinalBalance;
    }
}
