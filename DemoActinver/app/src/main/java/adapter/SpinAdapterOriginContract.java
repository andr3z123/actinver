package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nubaj.demoactinver.R;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import model.Datum;

/**
 * Created by andresaleman on 2/27/17.
 */

public class SpinAdapterOriginContract extends BaseAdapter
{
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Datum> values;
    LayoutInflater inflter;

    public SpinAdapterOriginContract(Context context, List<Datum> values)
    {
        this.context = context;
        this.values = values;
        inflter = (LayoutInflater.from(context));
    }

    public int getCount(){
        return values.size();
    }

    public Datum getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view = inflter.inflate(R.layout.spinner_transfer, null);
        TextView textAlias = (TextView) view.findViewById(R.id.textViewAlias);
        TextView textContract = (TextView) view.findViewById(R.id.textViewContract);
        TextView textTitleBalance = (TextView) view.findViewById(R.id.textViewAccountNumber);
        TextView textBalance = (TextView) view.findViewById(R.id.textViewBalance);
        textAlias.setText(values.get(i).getName());
        textContract.setText(String.valueOf(values.get(i).getContract()));
        textTitleBalance.setText("Saldo Disponible:");
        Double total = Double.parseDouble(values.get(i).getTotal());
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("MXN"));
        format.setMinimumFractionDigits(2);
        textBalance.setText(format.format(total));
        return view;
    }
}
