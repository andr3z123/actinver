package com.nubaj.demoactinver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import utils.GlobalVariables;

public class FinishActivity extends AppCompatActivity
{
    TextView textViewTitleAutorizathion, textViewAutorizathion, textViewTitleKey, textViewKey, textViewTitleDate, textViewDate, textViewTitleOriginContract, textViewOriginContract, textViewTitleDestinationContract, textViewDestinationContract, textViewTitleAmount, textViewAmount,textViewTitleDescription, textViewDescription;
    Button buttonFinish, buttonRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        textViewTitleAutorizathion = (TextView) findViewById(R.id.textViewTitleAutorizathion);
        textViewTitleAutorizathion.setText(getString(R.string.text_authorization_number));
        textViewTitleKey = (TextView) findViewById(R.id.textViewTitleKey);
        textViewTitleKey.setText(getString(R.string.text_track_id));
        textViewTitleDate = (TextView) findViewById(R.id.textViewTitleDate);
        textViewTitleDate.setText(getString(R.string.text_date_transfer_complete));
        textViewTitleOriginContract = (TextView) findViewById(R.id.textViewTitleOriginContract);
        textViewTitleOriginContract.setText(getString(R.string.text_contract_origin));
        textViewTitleDestinationContract = (TextView) findViewById(R.id.textViewTitleDestinationContract);
        textViewTitleDestinationContract.setText(getString(R.string.text_contract_destination));
        textViewTitleAmount = (TextView) findViewById(R.id.textViewTitleAmount);
        textViewTitleAmount.setText(getString(R.string.text_amount_transfer_complete));
        textViewTitleDescription = (TextView) findViewById(R.id.textViewTitleDescription);
        textViewTitleDescription.setText(getString(R.string.text_description_transfer_complete));

        textViewAutorizathion = (TextView) findViewById(R.id.textViewAutorizathion);
        textViewKey = (TextView) findViewById(R.id.textViewKey);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewOriginContract = (TextView) findViewById(R.id.textViewOriginContract);
        textViewDestinationContract = (TextView) findViewById(R.id.textViewDestinationContract);
        textViewAmount = (TextView) findViewById(R.id.textViewAmount);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);
        buttonFinish = (Button) findViewById(R.id.buttonFinish);
        buttonFinish.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                GlobalVariables.load = true;
                finish();
                finish();
                Intent intent = new Intent(FinishActivity.this,MovementsActivity.class);
                startActivity(intent);
            }
        });

        if(!TextUtils.isEmpty(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getOperationReference()))
        {
            textViewAutorizathion.setText(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getOperationReference().toString());
        }
        if(!TextUtils.isEmpty(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getTrackingCode()))
        {
            textViewKey.setText(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getTrackingCode().toString());
        }
        if(!TextUtils.isEmpty(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getOperationDateTime()))
        {
            textViewDate.setText(GlobalVariables.othersAccountsBankThirdpartyResponse.getData().getOperationDateTime().toString());
        }
        textViewOriginContract.setText(GlobalVariables.transferRequest.getContract()+","+GlobalVariables.transferRequest.getAlias().toString());
        textViewDestinationContract.setText(GlobalVariables.transferRequest.getBank_name()+","+GlobalVariables.transferRequest.getDestinationAccount().toString()+","+GlobalVariables.transferRequest.getDestination_name().toString());
        Double total = Double.parseDouble(String.valueOf(GlobalVariables.transferRequest.getAmount()));
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("MXN"));
        format.setMinimumFractionDigits(2);
        textViewAmount.setText(format.format(total)+" MN");
        textViewDescription.setText(GlobalVariables.transferRequest.getComments().toString());
    }
}
