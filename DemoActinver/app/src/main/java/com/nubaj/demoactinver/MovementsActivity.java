package com.nubaj.demoactinver;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

import fragment.MovementsFragment;
import fragment.TransferFragment;
import implementation.FunctionJson;
import model.ErrorResponse;
import model.LogoutResponse;
import utils.GlobalVariables;

public class MovementsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movements);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        showFragment();
    }

    private void write()
    {
        SharedPreferences sharedPref = MovementsActivity.this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.access_token_value), "");
        editor.commit();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    switch (which)
                    {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            FunctionJson functionJson = new FunctionJson();
                            HashMap<String, String> hashMapLogout = new HashMap<String, String>();
                            String response = "";
                            response = functionJson.RequestHttp(getString(R.string.url_logout)+GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapLogout);
                            Gson gson = new GsonBuilder().create();
                            GlobalVariables.logoutResponse = new LogoutResponse();
                            GlobalVariables.logoutResponse = gson.fromJson(response, LogoutResponse.class);
                            if(!TextUtils.isEmpty(response))
                            {
                                if (GlobalVariables.logoutResponse.isSuccess() == true)
                                {
                                    GlobalVariables.logout = true;
                                    write();
                                    finish();
                                    Intent intent = new Intent(MovementsActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(MovementsActivity.this, GlobalVariables.logoutResponse.getMessage().toString(), Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    try
                                    {
                                        ErrorResponse errorResponse = new ErrorResponse();
                                        errorResponse = gson.fromJson(response, ErrorResponse.class);
                                        Toast.makeText(MovementsActivity.this, errorResponse.getError_description().toString(), Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(MovementsActivity.this,MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    catch (Exception e)
                                    {
                                        Toast.makeText(MovementsActivity.this, getString(R.string.error_request_login), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                            else
                            {
                                finish();
                                Toast.makeText(MovementsActivity.this, getString(R.string.message_logout_2), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.message_logout)).setPositiveButton(getString(R.string.action_ok), dialogClickListener)
                    .setNegativeButton(getString(R.string.action_cancel), dialogClickListener).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.movements, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_movements)
        {
            fragment = new MovementsFragment();
            this.setTitle(getText(R.string.nav_movements));
            callFragment(fragment);
        }
        else if (id == R.id.nav_transfer)
        {
            fragment = new TransferFragment();
            this.setTitle(getText(R.string.nav_transfer));
            callFragment(fragment);
        }
        else if (id == R.id.nav_logout)
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    switch (which)
                    {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            FunctionJson functionJson = new FunctionJson();
                            HashMap<String, String> hashMapLogout = new HashMap<String, String>();
                            String response = "";
                            response = functionJson.RequestHttp(getString(R.string.url_logout)+GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapLogout);
                            Gson gson = new GsonBuilder().create();
                            GlobalVariables.logoutResponse = new LogoutResponse();
                            GlobalVariables.logoutResponse = gson.fromJson(response, LogoutResponse.class);
                            if(!TextUtils.isEmpty(response))
                            {
                                if (GlobalVariables.logoutResponse.isSuccess() == true) {
                                    GlobalVariables.logout = true;
                                    finish();
                                    Intent intent = new Intent(MovementsActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(MovementsActivity.this, GlobalVariables.logoutResponse.getMessage().toString(), Toast.LENGTH_LONG).show();
                                } else {
                                    try {
                                        ErrorResponse errorResponse = new ErrorResponse();
                                        errorResponse = gson.fromJson(response, ErrorResponse.class);
                                        Toast.makeText(MovementsActivity.this, errorResponse.getError_description().toString(), Toast.LENGTH_LONG).show();
                                        finish();
                                        Intent intent = new Intent(MovementsActivity.this,MainActivity.class);
                                        startActivity(intent);
                                    } catch (Exception e) {
                                        Toast.makeText(MovementsActivity.this, getString(R.string.error_request_login), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                            else
                            {
                                finish();
                                Toast.makeText(MovementsActivity.this, getString(R.string.message_logout_2), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.message_logout)).setPositiveButton(getString(R.string.action_ok), dialogClickListener)
                    .setNegativeButton(getString(R.string.action_cancel), dialogClickListener).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void callFragment(Fragment fragment)
    {
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_movements, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void showFragment()
    {
        Fragment fragment = null;

        fragment = new MovementsFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_movements, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        this.setTitle(getText(R.string.nav_movements));
    }
}
