package com.nubaj.demoactinver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scottyab.aescrypt.AESCrypt;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import implementation.FunctionJson;
import model.AsesoradosResponse;
import model.ChallengeResponse;
import model.DestinationAccountResponse;
import model.ErrorResponse;
import model.LoginResponse;
import model.LogoutResponse;
import model.OthersAccountsBankThirdpartyResponse;
import model.TokenResponse;
import model.TransferRequest;
import model.TransferResponse;
import utils.GlobalVariables;
import utils.InternetConnection;

public class MainActivity extends AppCompatActivity
{
    private AutoCompleteTextView editTextUser;
    private EditText editTextPassword;
    private HashMap<String, String> hashMapLogin;
    private FunctionJson functionJson;
    String prvvDate;
    Button buttonLogin;
    char character;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(GlobalVariables.logout==true)
        {
            write();
            GlobalVariables.access_token = false;
            GlobalVariables.logout =false;
            GlobalVariables.destinationAccountResponse = new DestinationAccountResponse();
            GlobalVariables.loginResponse = new LoginResponse();
            GlobalVariables.logoutResponse = new LogoutResponse();
            GlobalVariables.asesoradosResponse = new AsesoradosResponse();
            GlobalVariables.challengeResponse = new ChallengeResponse();
            GlobalVariables.cookiesHeader = new ArrayList<String>();
            GlobalVariables.othersAccountsBankThirdpartyResponse = new OthersAccountsBankThirdpartyResponse();
            GlobalVariables.tokenResponse = new TokenResponse();
            GlobalVariables.transferRequest = new TransferRequest();
            GlobalVariables.transferResponse = new TransferResponse();
        }
        SharedPreferences sharedPreferences = this.getPreferences(MODE_PRIVATE);
        String valueDate = "";
        prvvDate = sharedPreferences.getString(getString(R.string.access_token_value),valueDate);
        if(!TextUtils.isEmpty(prvvDate))
        {
            FunctionJson functionJson = new FunctionJson();
            HashMap<String, String> hashMapLogout = new HashMap<String, String>();
            String response = "";
            response = functionJson.RequestHttp(getString(R.string.url_logout)+prvvDate, FunctionJson.GET, hashMapLogout);
            Gson gson = new GsonBuilder().create();
            GlobalVariables.logoutResponse = new LogoutResponse();
            GlobalVariables.logoutResponse = gson.fromJson(response, LogoutResponse.class);
            if(!TextUtils.isEmpty(response))
            {
                if (GlobalVariables.logoutResponse.isSuccess() == true)
                {
                    write();
                    Toast.makeText(MainActivity.this, GlobalVariables.logoutResponse.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
                else
                {
                    try
                    {
                        ErrorResponse errorResponse = new ErrorResponse();
                        errorResponse = gson.fromJson(response, ErrorResponse.class);
                        Toast.makeText(MainActivity.this, errorResponse.getError_description().toString(), Toast.LENGTH_LONG).show();

                    }
                    catch (Exception e)
                    {
                        Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_LONG).show();
                    }
                }
            }
            else
            {
                Toast.makeText(MainActivity.this, getString(R.string.message_logout_2), Toast.LENGTH_LONG).show();
            }
        }

        editTextUser = (AutoCompleteTextView) findViewById(R.id.editTextUser);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View focusView = null;

                focusView = getCurrentFocus();

                // Reset errors.
                editTextUser.setError(null);
                editTextPassword.setError(null);
                if (focusView != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                }
                if(InternetConnection.checkConnection(MainActivity.this))
                {

                    if (!TextUtils.isEmpty(editTextUser.getText().toString()) && !TextUtils.isEmpty(editTextPassword.getText().toString())) {
                        new enviar_informacion().execute();
                    } else {
                        if (TextUtils.isEmpty(editTextUser.getText().toString()) && TextUtils.isEmpty(editTextPassword.getText().toString())) {
                            editTextUser.setError(getString(R.string.error_user));
                            focusView = editTextUser;
                        } else {
                            if (TextUtils.isEmpty(editTextUser.getText().toString())) {
                                editTextUser.setError(getString(R.string.error_user));
                                focusView = editTextUser;
                            } else {
                                editTextPassword.setError(getString(R.string.error_password));
                                focusView = editTextPassword;
                            }
                        }
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this,getString(R.string.error_internet),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void write()
    {
        SharedPreferences sharedPref = MainActivity.this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.access_token_value), "");
        editor.commit();
    }



    public class enviar_informacion extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("Iniciar Sesión");
            pDialog.setMessage("Espere un momento mientras inicia la sesión...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onLogin();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();
            if(GlobalVariables.loginResponse!=null)
            {
                if (!TextUtils.isEmpty(GlobalVariables.loginResponse.getAccess_token()))
                {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            new token_type().execute();
                        }
                    });
                }
            }
        }

        private void write()
        {
            SharedPreferences sharedPref = MainActivity.this.getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.access_token_value), GlobalVariables.loginResponse.getAccess_token().toString());
            editor.commit();
        }

        void onLogin()
        {
            functionJson = new FunctionJson();
            hashMapLogin = new HashMap<String, String>();
            try
            {
                hashMapLogin.put("username", editTextUser.getText().toString().toUpperCase());
                hashMapLogin.put("password", editTextPassword.getText().toString());
                hashMapLogin.put("scope", getString(R.string.parameters_scope));
                hashMapLogin.put("client", getString(R.string.parameters_client));
                hashMapLogin.put("grantType", getString(R.string.parameters_grandType));
                hashMapLogin.put("clientSecret", getString(R.string.parameters_clientSecret));
                String response = "";
                response = functionJson.RequestHttp(getString(R.string.url_login), FunctionJson.POST, hashMapLogin);
                Gson gson = new GsonBuilder().create();
                if(!TextUtils.isEmpty(response))
                {
                    GlobalVariables.loginResponse = new LoginResponse();
                    GlobalVariables.loginResponse = gson.fromJson(response, LoginResponse.class);
                    if(!TextUtils.isEmpty(GlobalVariables.loginResponse.getAccess_token()))
                    {
                        write();
                    }
                    else
                    {
                        if(!TextUtils.isEmpty(response))
                        {
                            ErrorResponse errorResponse = new ErrorResponse();
                            errorResponse = gson.fromJson(response, ErrorResponse.class);
                            final ErrorResponse finalErrorResponse = errorResponse;
                            MainActivity.this.runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                    Toast.makeText(MainActivity.this, finalErrorResponse.getError_description(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else
                        {
                            MainActivity.this.runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                    Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }
                }
                else
                {
                    MainActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.getStackTrace().toString();
                MainActivity.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    public class token_type extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("Iniciar Sesión");
            pDialog.setMessage("Espere un momento mientras inicia la sesión...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onTokenType();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        void onTokenType()
        {
            functionJson = new FunctionJson();
            hashMapLogin = new HashMap<String, String>();
            try
            {
                String response = "";
                response = functionJson.RequestHttp(getString(R.string.url_token_type)+GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapLogin);
                Gson gson = new GsonBuilder().create();
                if(!TextUtils.isEmpty(response))
                {
                    GlobalVariables.tokenResponse = new TokenResponse();
                    GlobalVariables.tokenResponse = gson.fromJson(response, TokenResponse.class);
                    if(GlobalVariables.tokenResponse.isSuccess()==true)
                    {
                        finish();
                        Intent intent = new Intent(MainActivity.this, MovementsActivity.class);
                        //Intent intent = new Intent(MainActivity.this, ConfirmActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        if(!TextUtils.isEmpty(response))
                        {
                            ErrorResponse errorResponse = new ErrorResponse();
                            errorResponse = gson.fromJson(response, ErrorResponse.class);
                            final ErrorResponse finalErrorResponse = errorResponse;
                            MainActivity.this.runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                    Toast.makeText(MainActivity.this, finalErrorResponse.getError_description(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else
                        {
                            MainActivity.this.runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                    Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }
                }
                else
                {
                    MainActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.getStackTrace().toString();
                MainActivity.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(MainActivity.this, getString(R.string.error_request_login), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
