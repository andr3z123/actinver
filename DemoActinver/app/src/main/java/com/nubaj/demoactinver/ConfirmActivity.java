package com.nubaj.demoactinver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.Key;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import implementation.FunctionJson;
import model.ChallengeResponse;
import model.ErrorSever;
import model.OthersAccountsBankThirdpartyResponse;
import model.ThirdRequest;
import model.ValidateResponse;
import utils.GlobalVariables;
import utils.InternetConnection;

import static android.content.ContentValues.TAG;

public class ConfirmActivity extends AppCompatActivity
{
    TextView textViewTitleChallenge, textViewChallenge;
    EditText editTextToken;
    Button buttonTransfer, button;
    String response="",id="";
    byte[]decryptResult;
    int REQUEST_CODE=1;
    FunctionJson functionJson;
    HashMap<String, String> hashMapTransfer, hashMapValidate, hashMapChallenge;
    Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        setTitle(getString(R.string.title_confirm));
        textViewChallenge = (TextView) findViewById(R.id.textViewTitleChallenge);
        textViewTitleChallenge = (TextView) findViewById(R.id.textViewChallenge);
        editTextToken = (EditText) findViewById(R.id.editTextToken);
        //if(GlobalVariables.tokenResponse.getData().equals("SOFT"))
        if(GlobalVariables.tokenResponse.getData().equals("SOFT"))
        {
            editTextToken.setHint(getString(R.string.text_soft));
            onChallenge();
            //textViewTitleChallenge.setText(GlobalVariables.challengeResponse.getData().getTokenChallenge().toString());
            //textViewTitleChallenge.setVisibility(View.GONE);
            //textViewChallenge.setVisibility(View.GONE);
        }
        else
        {
            editTextToken.setHint(getString(R.string.text_hard));
            textViewTitleChallenge.setText(GlobalVariables.challengeResponse.getData().getTokenChallenge().toString());
        }

        buttonTransfer = (Button) findViewById(R.id.buttonTransfer);
        buttonTransfer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                View focusView = null;

                focusView = getCurrentFocus();

                // Reset errors.
                editTextToken.setError(null);
                if (focusView != null)
                {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                }
                if(!TextUtils.isEmpty(editTextToken.getText().toString()))
                {
                    if(GlobalVariables.tokenResponse.getData().equals("SOFT"))
                    {
                        if (InternetConnection.checkConnection(ConfirmActivity.this))
                        {
                            generateActiPass(editTextToken.getText().toString(), textViewTitleChallenge.getText().toString());
                        }
                        else
                        {
                            Toast.makeText(ConfirmActivity.this, getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        if (InternetConnection.checkConnection(ConfirmActivity.this)) {
                            new ConfirmActivity.validate().execute();
                        } else {
                            Toast.makeText(ConfirmActivity.this, getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else
                {
                    editTextToken.setError(getString(R.string.error_token));
                    focusView = editTextToken;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //super.onActivityResult(requestCode, resultCode, data);
        String result="";
        String reconstitutedString="";
        if(REQUEST_CODE == requestCode)
        {
            if(data.getExtras()!=null)
            {
                try
                {
                    result = decrypt(data.getExtras().getString("TOKEN_DATA"));
                    for(int i = 0; i < result.length(); i++)
                    {
                        if(Character.isDigit(result.charAt(i)))
                        {
                            reconstitutedString+=result.charAt(i);
                            //Do whatever you like.
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                switch (reconstitutedString)
                {
                    case"1111111111111111":
                        Toast.makeText(this, "EL PIN que ingresaste es incorrecto, recuerda que solo...", Toast.LENGTH_SHORT).show();
                        break;
                    case"0000000000000000":

                        Toast.makeText(this, "En necesario enrolar la cuenta...", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        editTextToken.setText(reconstitutedString);
                        if (InternetConnection.checkConnection(ConfirmActivity.this))
                        {
                            new ConfirmActivity.validate().execute();
                        }
                        else
                        {
                            Toast.makeText(ConfirmActivity.this, getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        }
    }

    void onChallenge()
    {
        functionJson = new FunctionJson();
        hashMapChallenge = new HashMap<String, String>();
        response = functionJson.RequestHttp(getString(R.string.url_challenge) + GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapChallenge);
        gson = new GsonBuilder().create();
        try
        {
            GlobalVariables.challengeResponse = new ChallengeResponse();
            GlobalVariables.challengeResponse = gson.fromJson(response, ChallengeResponse.class);
            textViewTitleChallenge.setText(GlobalVariables.challengeResponse.getData().getTokenChallenge().toString());
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        GlobalVariables.validate = false;
        GlobalVariables.access_token=false;
        GlobalVariables.json = null;
    }

    public class enviar_informacion extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ConfirmActivity.this);
            pDialog.setTitle("Realizando transferencia");
            pDialog.setMessage("Espere un momento mientras se realiza la transferencia...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onDestination();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        void onDestination()
        {
            response = "";
            functionJson = new FunctionJson();
            hashMapTransfer = new HashMap<String, String>();
            /*hashMapTransfer.put("destinationAccount",GlobalVariables.transferRequest.getDestinationAccount());
            hashMapTransfer.put("transferAmount",String.valueOf(GlobalVariables.transferRequest.getAmount()));
            hashMapTransfer.put("transferCurrency",GlobalVariables.transferRequest.getCurrency());
            hashMapTransfer.put("transferDetails",GlobalVariables.transferRequest.getComments());
            hashMapTransfer.put("comments",GlobalVariables.transferRequest.getComments());
            hashMapTransfer.put("newValue",editTextToken.getText().toString());*/
            ThirdRequest thirdRequest = new ThirdRequest();
            if(GlobalVariables.tokenResponse.getData().equals("SOFT"))
            {
                thirdRequest.setAccountType(GlobalVariables.transferRequest.getAccountType());
                hashMapTransfer.put("bank", GlobalVariables.transferRequest.getBank());
            }

            thirdRequest.setDestinationAccount(GlobalVariables.transferRequest.getDestinationAccount());
            thirdRequest.setTransferAmount(GlobalVariables.transferRequest.getAmount());
            thirdRequest.setTransferCurrency(GlobalVariables.transferRequest.getCurrency());
            thirdRequest.setTransferDetails(GlobalVariables.transferRequest.getComments());
            thirdRequest.setComments(GlobalVariables.transferRequest.getComments());
            thirdRequest.setNewValue(editTextToken.getText().toString());
            thirdRequest.setBank(GlobalVariables.transferRequest.getBank());
            Gson gson = new GsonBuilder().create();
            GlobalVariables.json = gson.toJson(thirdRequest);
            response="";
            GlobalVariables.access_token = true;
            //response = functionJson.RequestHttp(getString(R.string.url_transfer)+GlobalVariables.transferRequest.getContract().toString()+"/transfer/others/thirdparty/bank?access_token="+GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.POST, hashMapTransfer);
            response = functionJson.RequestHttp(getString(R.string.url_transfer)+GlobalVariables.transferRequest.getContract().toString()+"/transfer/others/thirdparty/bank", FunctionJson.POST, hashMapTransfer);
            try
            {
                GlobalVariables.othersAccountsBankThirdpartyResponse = new OthersAccountsBankThirdpartyResponse();
                GlobalVariables.othersAccountsBankThirdpartyResponse = gson.fromJson(response, OthersAccountsBankThirdpartyResponse.class);
                if(GlobalVariables.othersAccountsBankThirdpartyResponse.isSuccess()==true)
                {
                    ConfirmActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Intent intent = new Intent(ConfirmActivity.this,FinishActivity.class);
                            startActivity(intent);
                        }
                    });
                }
                else
                {
                    ConfirmActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(ConfirmActivity.this,GlobalVariables.othersAccountsBankThirdpartyResponse.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSever errorSever = new ErrorSever();
                errorSever = gson.fromJson(response, ErrorSever.class);
                ex.getStackTrace();
                final ErrorSever finalErrorSever = errorSever;
                ConfirmActivity.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {

                        Toast.makeText(ConfirmActivity.this, finalErrorSever.getMessage().toString(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }


    public class validate extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ConfirmActivity.this);
            pDialog.setTitle("Realizando transferencia");
            pDialog.setMessage("Espere un momento mientras se realiza la transferencia...");
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            onDestination();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();
            if(GlobalVariables.validateResponse.isSuccess()==true)
            {
                ConfirmActivity.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        new ConfirmActivity.enviar_informacion().execute();
                    }
                });
            }

        }

        void onDestination()
        {
            response = "";
            functionJson = new FunctionJson();
            hashMapValidate = new HashMap<String, String>();
            response="";
            GlobalVariables.validate = true;
            response = functionJson.RequestHttp(getString(R.string.url_validate)+GlobalVariables.loginResponse.getAccess_token().toString(), FunctionJson.GET, hashMapTransfer);
            Gson gson = new GsonBuilder().create();
            try
            {
                GlobalVariables.validateResponse = new ValidateResponse();
                GlobalVariables.validateResponse = gson.fromJson(response,ValidateResponse.class);
                if(GlobalVariables.validateResponse.isSuccess()==true)
                {
                    ConfirmActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(ConfirmActivity.this,GlobalVariables.validateResponse.getMessage().toString(),Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else
                {
                    ConfirmActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(ConfirmActivity.this,GlobalVariables.validateResponse.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
        }
    }

    void generateActiPass(String pin, String challenge)
    {
        try
        {
            id = Settings.Secure.getString(getBaseContext().getContentResolver(), "android_id");
            Intent intent = new Intent("mx.actinver.actinver.GENERATED_TOKEN");
            intent.putExtra("PIN_DATA", encrypt(pin)); //Byte[]
            intent.putExtra("CHALLENGE_DATA",encrypt(challenge)); //Byte[]
            if(intent.resolveActivity(ConfirmActivity.this.getPackageManager())!=null)
            {
                startActivityForResult(intent,REQUEST_CODE);
            }
            else
            {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "mx.actinver.actinver.GENERATED_TOKEN")));
                Log.d(TAG,"ACTIPASS is not installed");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static final String ALGORITMO = "AES";
    private static byte[] valor_clave;

    String encrypt(String text_to_encrypt) throws Exception
    {
        try
        {
            valor_clave = id.getBytes();
            Key key = new SecretKeySpec(valor_clave, ALGORITMO);
            Cipher cipher = Cipher.getInstance(ALGORITMO);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return Base64.encodeToString(cipher.doFinal(text_to_encrypt.getBytes("UTF-8")), Base64.DEFAULT);
        }
        catch (Exception e)
        {
            return  null;
        }
    }

    String decrypt(String text_encrypt) throws Exception
    {
        try
        {
            valor_clave = id.getBytes();
            Key key = new SecretKeySpec(valor_clave, ALGORITMO);
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] clearText = cipher.doFinal(Base64.decode(text_encrypt, Base64.DEFAULT));
            return new String(clearText, "UTF-8");
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
