package implementation;

import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import utils.GlobalVariables;

/**
 * Created by andresaleman on 2/23/17.
 */

//Clase para Peticiones Web
public class FunctionJson
{
    public final static int GET = 1;
    public final static int POST = 2;

    public String RequestHttp(String url, int method, HashMap<String,String> params)
    {
        String response="";

        try
        {
            if(method==1)
            {
                response = GetResponse(url);
            }
            else if(method==2)
            {
                response = postResponse(url,params);
            }
        }
        catch (Exception ex)
        {

        }
        return response;
    }

    private String GetResponse(String url) throws Exception
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        String responseGet="";
        try
        {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");
            //con.setRequestProperty("Authorization", GlobalVariables.loginResponse.getToken_type()+" "+GlobalVariables.loginResponse.getAccess_token());

            int responseCode = con.getResponseCode();

            GlobalVariables.status_code = responseCode;

            if(GlobalVariables.validate==true)
            {
                if(!TextUtils.isEmpty(con.getHeaderField("X-CSRF-TOKEN").toString()))
                {
                    GlobalVariables.access_token_string = con.getHeaderField("X-CSRF-TOKEN");
                }

                String header = con.getHeaderField("Set-Cookie");
                String COOKIES_HEADER = "Set-Cookie";
                java.net.CookieManager msCookieManager = new java.net.CookieManager();

                Map<String, List<String>> headerFields = con.getHeaderFields();
                GlobalVariables.cookiesHeader = headerFields.get(COOKIES_HEADER);

                if (GlobalVariables.cookiesHeader != null) {
                    for (String cookie : GlobalVariables.cookiesHeader) {
                        msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                    }
                }
                GlobalVariables.validate = false;
                GlobalVariables.set_cookie = con.getHeaderField("Set-Cookie");
                Log.d("X-CSRF-TOKEN:", GlobalVariables.access_token_string);
            }
            else
            {
                //Log.d("X-CSRF-TOKEN Anterior:", con.getHeaderField("X-CSRF-TOKEN").toString());
            }
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK)
            {

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                    responseGet += inputLine;
                }
                in.close();
            }
            else
            {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getErrorStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                    responseGet += inputLine;
                }
                in.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return responseGet;

    }

    static final String COOKIE = "Cookie";

    public String postResponse(String requestURL, HashMap<String, String> postDataParams)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        URL url;
        String response = "";
        String query="";

        try
        {
            url = new URL(requestURL);



            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(500000);
            conn.setConnectTimeout(500000);
            //conn.setDoOutput(true);
            if(GlobalVariables.access_token==true)
            {
                conn.setUseCaches (false);
                conn.addRequestProperty("Authorization", GlobalVariables.loginResponse.getToken_type()+" "+GlobalVariables.loginResponse.getAccess_token());
                if (GlobalVariables.cookiesHeader.size() > 0) {
                    //While joining the Cookies, use ',' or ';' as needed. Most of the server are using ';'
                    conn.setRequestProperty(COOKIE ,
                            TextUtils.join(";", GlobalVariables.cookiesHeader));
                }
                conn.setRequestProperty("Content-Type","application/json; charset=utf8");
                conn.setRequestProperty("X-CSRF-TOKEN",GlobalVariables.access_token_string.toString());
                //conn.setRequestProperty("User-Agent",System.getProperty("http.agent").toString());
            }
            conn.setUseCaches(false);
            HttpURLConnection.setFollowRedirects(false);
            conn.setAllowUserInteraction(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");


            conn.connect();

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            if(TextUtils.isEmpty(GlobalVariables.json))
            {
                writer.write(SerializeJson(postDataParams));
            }
            else
            {
                writer.write(GlobalVariables.json);
                System.out.println("\nJson Transfer : " + GlobalVariables.json.toString());
            }


            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            GlobalVariables.status_code = responseCode;


            if (responseCode == HttpsURLConnection.HTTP_OK)
            {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null)
                {
                    response+=line;
                }
                Log.d("access_token",response.toString());
            }
            else
            {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                while ((line=br.readLine()) != null)
                {
                    response+=line;
                }
            }
            conn.disconnect();
        } catch (Exception e)
        {
            e.printStackTrace();
            Log.d("access_token",response.toString());
        }

        return response;
    }

    private String SerializeJson(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        JSONObject jsonObject = new JSONObject();
        boolean first = true;
        try
        {
            for (Map.Entry<String, String> entry : params.entrySet())
            {
                if (first)
                    first = false;
                else
                    result.append("&");

                jsonObject.put(entry.getKey(), entry.getValue());
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        String json = jsonObject.toString();

        return json;
    }
}
