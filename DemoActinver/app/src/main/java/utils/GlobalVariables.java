package utils;

import java.util.List;

import model.AsesoradosResponse;
import model.ChallengeResponse;
import model.DestinationAccountResponse;
import model.LoginResponse;
import model.LogoutResponse;
import model.MovementsResponse;
import model.OthersAccountsBankThirdpartyResponse;
import model.TokenResponse;
import model.TransferRequest;
import model.TransferResponse;
import model.ValidateResponse;

/**
 * Created by andresaleman on 2/23/17.
 */

public class GlobalVariables
{
    // Variables Globales.
    public static LoginResponse loginResponse;
    public static LogoutResponse logoutResponse;
    public static AsesoradosResponse asesoradosResponse;
    public static MovementsResponse movementsResponse;
    public static DestinationAccountResponse destinationAccountResponse;
    public static ChallengeResponse challengeResponse;
    public static TransferRequest transferRequest;
    public static ValidateResponse validateResponse;
    public static TransferResponse transferResponse;
    public static OthersAccountsBankThirdpartyResponse othersAccountsBankThirdpartyResponse;
    public static TokenResponse tokenResponse;
    public static String json="", token_type="SOFT";
    public static boolean access_token=false, validate=false, load=false, logout=false;
    public static String access_token_string="";
    public static String X_CSRF_TOKEN="";
    public static String set_cookie;
    public static String user_agent;
    public static List<String> cookiesHeader;
    public static int status_code=0;
}
